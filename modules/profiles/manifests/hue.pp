
class profiles::hue(
  $domain                 = lookup('domain'),
  $hdfs_hostname          = lookup('hadoop::hdfs_hostname'),
  $yarn_hostname          = lookup('hadoop::yarn_hostname'),
  $hdfs_hostname2         = lookup('hadoop::hdfs_hostname2'),
  $yarn_hostname2         = lookup('hadoop::yarn_hostname2'),
  $historyserver_hostname = lookup('hadoop::yarn_hostname'),
  $hue_hostnames          = lookup('hadoop::hue_hostnames'),
  $httpfs_hostnames       = lookup('hadoop::httpfs_hostnames'),
  $slaves                 = lookup('hadoop::slaves'),
  $frontends              = lookup('hadoop::frontends'),
  $cluster_name           = lookup('hadoop::cluster_name'),
  ) {

    $slaves_fqdn = $slaves.map |$s| { "${s}.${domain}" }
    $frontends_fqdn = $frontends.map |$s| { "${s}.${domain}" }
    $hue_hostnames_fqdn = $hue_hostnames.map |$s| { "${s}.${domain}" }
    $httpfs_hostnames_fqdn = $httpfs_hostnames.map |$s| { "${s}.${domain}" }

    include profiles::hadoop_common
    include ::hadoop::frontend
    include ::hadoop::httpfs
    include ::hue::user

    file {"/etc/hadoop-httpfs/conf/log4j.properties":
      ensure => 'file',
      owner  => 'root',
      group  => 'root',
      source => 'puppet:///modules/profiles/log4j.properties',
      before => Class['hadoop::httpfs::service'],
    }

    class{'::hue':
      db_password         => 'huepassword',
      db_host             => 'localhost',
      db_user             => 'hue',
      httpfs_hostname     => 'localhost',
      defaultFS           => lookup('hadoop::cluster_name'),
      zookeeper_hostnames => $quorum_hostnames,
      yarn_hostname       => "${yarn_hostname}.${domain}",
      yarn_hostname2      => "${yarn_hostname2}.${domain}",
      secret              => 'jettehemlig!',
      cluster_name        => lookup('hadoop::cluster_name'),
    }
}

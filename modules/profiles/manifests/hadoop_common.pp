class profiles::hadoop_common(
  $domain                 = lookup('domain'),
  $hdfs_hostname          = lookup('hadoop::hdfs_hostname'),
  $yarn_hostname          = lookup('hadoop::yarn_hostname'),
  $hdfs_hostname2         = lookup('hadoop::hdfs_hostname2'),
  $yarn_hostname2         = lookup('hadoop::yarn_hostname2'),
  $historyserver_hostname = lookup('hadoop::yarn_hostname'),
  $hue_hostnames          = lookup('hadoop::hue_hostnames'),
  $httpfs_hostnames       = lookup('hadoop::httpfs_hostnames'),
  $slaves                 = lookup('hadoop::slaves'),
  $frontends              = lookup('hadoop::frontends'),
  $cluster_name           = lookup('hadoop::cluster_name'),
) {

  $slaves_fqdn = $slaves.map |$s| { "${s}.${domain}" }
  $frontends_fqdn = $frontends.map |$s| { "${s}.${domain}" }
  $hue_hostnames_fqdn = $hue_hostnames.map |$s| { "${s}.${domain}" }
  $httpfs_hostnames_fqdn = $httpfs_hostnames.map |$s| { "${s}.${domain}" }

  class { "hadoop":
    hdfs_hostname          => "${hdfs_hostname}.${domain}",
    yarn_hostname          => "${yarn_hostname}.${domain}",
    hdfs_hostname2         => "${hdfs_hostname2}.${domain}",
    yarn_hostname2         => "${yarn_hostname2}.${domain}",
    journalnode_hostnames  => $quorum_hostnames,
    zookeeper_hostnames    => $quorum_hostnames,
    historyserver_hostname => "${yarn_hostname}.${domain}",
    hue_hostnames          => $hue_hostnames,
    httpfs_hostnames       => $httpfs_hostnames,
    slaves                 => $slaves_fqdn,
    frontends              => $frontends,
    cluster_name           => "${cluster_name}.${domain}",
    properties             => {
      'dfs.replication'              => 2,
      'hadoop.proxyuser.hue.hosts'   => 'hue.sky.net, hue',
      'hadoop.proxyuser.super.users' => 'hue',
    },
  }
}

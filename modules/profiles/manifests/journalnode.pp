class profiles::journalnode {

  class { 'zookeeper':
    hostnames => $quorum_hostnames,
  }

  include ::zookeeper::server
  include hadoop::journalnode

}
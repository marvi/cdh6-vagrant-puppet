class profiles::common_namenode {

  class { 'zookeeper':
    hostnames => $quorum_hostnames,
    before => Class['hadoop::zkfc'],
  }
  include ::zookeeper::server

  include ::hue::user


}
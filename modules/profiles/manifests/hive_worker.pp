class profiles::hive_worker(
  $hdfs_hostname = lookup('hadoop::hdfs_hostname'),
  $domain        = lookup('domain'),
) {

  class{"hive":
    hdfs_hostname      => lookup('hadoop::cluster_name'),
    metastore_hostname => "${hdfs_hostname}.${domain}",
    server2_hostname   => "${hdfs_hostname}.${domain}",
  } 

  include ::hive::worker
  include ::hive::hdfs

}

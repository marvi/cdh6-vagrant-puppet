class profiles::namenode2 {

  include profiles::common_namenode

  notify { 'Identify host':
    name     => 'Namenode 2',
    message  => 'Installing Namenode',
  }

  include profiles::hadoop_common
  include hadoop::namenode
  include hadoop::resourcemanager
  include hadoop::zkfc
  include hadoop::journalnode
  include ::hue::user

}

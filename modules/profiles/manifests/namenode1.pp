class profiles::namenode1 {

  include profiles::common_namenode

  notify { 'Identify host':
    name     => 'Namenode 1',
    message  => 'Installing Namenode',
  }

  include profiles::hadoop_common
  include hadoop::namenode
  include hadoop::resourcemanager
  include hadoop::historyserver
  include hadoop::zkfc
  include hadoop::journalnode
  include hadoop::frontend
  include ::hue::user

  $touchfile = 'hue-dir-created'
  hadoop::mkdir { '/user/hue':
    mode      => 755,
    owner     => 'hue',
    group     => hue,
    touchfile => $touchfile,
  }

}

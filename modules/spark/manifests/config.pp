class spark::config {


  file {'/etc/spark/conf/spark-defaults.conf':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('spark/spark-defaults.conf.erb'),
  }

  file { 'spark-env.sh':


  }

}
class common::network(
  $domain  = lookup('domain'),
  $dns1     = lookup('dns1'),
  $dns2     = lookup('dns2'),
  $api_key = lookup('api_key'),
) {

  class {'do_dns':
    api_key  => $api_key,
    dns_zone => $domain,
  }

  file { '/etc/resolv.conf':
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template("common/resolv.conf.erb"),
  }

  file { '/etc/sysctl.d/disableipv6.conf':
    ensure => 'file',
    owner  => 'root',
    group  => 'root',
    mode   => '644',
    source => 'puppet:///modules/common/disableipv6.conf',
  }

}

class common::packages {

  include epel

  package { 'java-1.8.0-openjdk-headless':
    ensure => 'latest',
    #before => Class['profiles::hadoop_common'],
  }

  package { 'vim':
    ensure => 'latest',
  }

}

class common::ssh(
  $pub_key = lookup('pub_key'),
  $username = lookup('username'),
  $password = lookup('password'),
) {

  file { "/home/${username}/.ssh":
    ensure => 'directory',
    owner  => $username,
    group  => $username,
    mode   => '0700',
    require => User[$username],
  }

  ssh_authorized_key { 'marvi@pax':
    ensure  => present,
    user    => $username,
    type    => 'ssh-rsa',
    key     => $pub_key,
    require => User[$username],
  }

}
class common::firewall {

  service { 'firewalld':
    ensure => 'stopped',
    enable => false,
    #before => Class['profiles::hadoop_common'],
  }

}

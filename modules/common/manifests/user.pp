class common::user(
  $username = lookup('username'),
  $password = lookup('password'),
) {

  group { $username:
    ensure => 'present',
  } ->
  user { $username:
    ensure  => 'present',
    comment => 'Admin user',
    groups  => ['wheel', $username],
    managehome => true,
  }

}
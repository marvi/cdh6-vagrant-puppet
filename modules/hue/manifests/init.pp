class hue(
  $cluster_name = undef,
  $hdfs_hostname = undef,
  $httpfs_hostname = undef,
  $hive_server2_hostname = undef,
  $historyserver_hostname = undef,
  $impala_hostname = undef,
  $oozie_hostname = undef,
  $yarn_hostname = undef,
  $yarn_hostname2 = undef,
  $zookeeper_hostnames = [],
  $zookeeper_rest_hostname = undef,
  $auth = undef,
  $db_host = 'localhost',
  $db_user = 'hue',
  $db_name = undef,
  $db_password = undef,
  $defaultFS = undef,
  $environment = undef,
  $group = 'users',
  $keytab_hue = '/etc/security/keytab/hue.service.keytab',
  $keytab_spnego = '/etc/security/keytab/hue-http.service.keytab',
  $properties = undef,
  $realm = undef,
  $secret = '',
  $homedir      = '/var/lib/hue',
  $confdir      = '/etc/hue/conf',
  $defaultdir   = '/etc/default',
  $package_name = 'hue',
  $service_name = 'hue',
) {

  include ::stdlib
  validate_array($zookeeper_hostnames)

  # if (!$hdfs_hostname or empty($hdfs_hostname)) {
  #   if (!$defaultFS or empty($defaultFS)) {
  #     fail('$hdfs_hostname or $defaultFS parameter is required')
  #   }
  #   if (!$httpfs_hostname or empty($httpfs_hostname)) {
  #     fail('$hdfs_hostname or $httpfs_hostname parameter is required')
  #   }
  # }

  if $realm and !empty($realm) {
    $security_enabled = 'True'
  } else {
    $security_enabled = 'False'
  }


  $db_properties = {
    'desktop.database.engine'    => 'mysql',
    'desktop.database.host'      => $db_host,
    'desktop.database.name'      => pick($db_name, 'hue'),
    'desktop.database.user'      => $db_user,
    'desktop.database.password'  => 'huepassword',
  }

  if $realm and !empty($realm) {
    $security_properties = {
      'desktop.kerberos.hue_keytab' => $keytab_hue,
      'desktop.kerberos.hue_principal' => "hue/${::fqdn}@${realm}",
      'desktop.kerberos.kinit_path' => '/usr/bin/kinit',
      'hadoop.hdfs_clusters.default.security_enabled' => $security_enabled,
    }
  } else {
    $security_properties = {}
  }

  $hdfs_properties = {
    'hadoop.hdfs_clusters.default.fs_defaultfs' => $defaultFS,
    'hadoop.hdfs_clusters.default.webhdfs_url'  => "http://${httpfs_hostname}:14000/webhdfs/v1/",
  }

  $hive_base_properties = {
    'beeswax.hive_server_host' => $hive_server2_hostname,
  }

  $impala_base_properties = {
    # usefull for authorization (implemented only partially, HDFS
    # impersonation not avaialable in impala)
    'impala.impersonation_enabled' => 'True',
    'impala.server_host' => $impala_hostname,
    # port needs to be always specified
    'impala.server_port' => 21050,
  }

  $oozie_url = "http://${oozie_hostname}:11000/oozie"

  $_jhs_hostname = pick($historyserver_hostname, $yarn_hostname2, $yarn_hostname)
  $jhs_url = "http://${_jhs_hostname}:19888"
  $rm1_url = "http://${yarn_hostname}:8088"
  $rm2_url = "http://${yarn_hostname2}:8088"

  notify { "yarn_hostname: ${yarn_hostname} yarn_hostname2: ${yarn_hostname2}" : }

  $yarn_base_properties = {
    'hadoop.yarn_clusters.default.history_server_api_url' => $jhs_url,
    'hadoop.yarn_clusters.default.proxy_api_url' => $rm1_url,
    'hadoop.yarn_clusters.default.resourcemanager_api_url' => $rm1_url,
    'hadoop.yarn_clusters.default.resourcemanager_host' => $yarn_hostname,
    'hadoop.yarn_clusters.default.security_enabled' => $security_enabled,
    'hadoop.yarn_clusters.default.submit_to' => 'True',
  }

  $yarn_ha_properties = {
    'hadoop.yarn_clusters.default.logical_name' => 'rm1',
    'hadoop.yarn_clusters.ha.history_server_api_url' => $jhs_url,
    'hadoop.yarn_clusters.ha.logical_name' => 'rm2',
    'hadoop.yarn_clusters.ha.proxy_api_url' => $rm2_url,
    'hadoop.yarn_clusters.ha.resourcemanager_api_url' => $rm2_url,
    'hadoop.yarn_clusters.ha.resourcemanager_host' => $yarn_hostname2,
    'hadoop.yarn_clusters.ha.security_enabled' => $security_enabled,
    'hadoop.yarn_clusters.ha.submit_to' => 'True',
  }

  $yarn_properties = merge($yarn_base_properties, $yarn_ha_properties)

  $zoo_properties = {
    'zookeeper.clusters.default.host_ports' => "${join($zookeeper_hostnames, ':2181,')}:2181",
  }


  if $realm and !empty($realm) {
    $_keytab_spnego = '/etc/hue/hue-http.service.keytab'
    $auth_env = {
      'KRB5_KTNAME' => $_keytab_spnego,
    }
    $auth_properties = {
      'desktop.auth.backend' => 'desktop.auth.backend.SpnegoDjangoBackend',
    }
  }

  $_properties = merge($base_properties, $auth_properties, $db_properties, $security_properties, $hdfs_properties, $hive_properties, $impala_properties, $oozie_properties, $yarn_properties, $zoo_properties, $properties)

  class { '::hue::install': }
  -> class { '::hue::db': }
  -> class { '::hue::config': }
  ~> class { '::hue::service': }
  -> Class['::hue']

}
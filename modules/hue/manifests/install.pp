class hue::install {

  package {'hue':
    ensure => 'latest',
  }

  file { '/etc/init.d/hue':
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => 'puppet:///modules/hue/hue',
    require => Package['hue'],
  }

}
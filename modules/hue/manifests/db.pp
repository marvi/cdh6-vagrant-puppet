class hue::db {

  class { '::mysql::server':
    root_password => 'strongpassword',
  }

  mysql::db { 'hue':
    user     => 'hue',
    password => 'huepassword',
    grant    => ['ALL'],
  }


}
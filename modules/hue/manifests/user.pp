
class hue::user {

  group { 'hue':
    ensure => present,
    system => true,
  }

  user { 'hue':
    ensure     => present,
    system     => true,
    comment    => 'Hue daemon',
    gid        => 'hue',
    home       => '/usr/lib/hue',
    managehome => true,
    password   => '!!',
    shell      => '/bin/false',
  }
  Group['hue'] -> User['hue']

}
# == Class hadoop::httpfs::install
#
class hadoop::httpfs::install {
  include ::stdlib
  contain hadoop::common::install

  ensure_packages($hadoop::packages_httpfs)

  ::hadoop_lib::postinstall{ 'hadoop-httpfs':
    alternatives => $::hadoop::alternatives_httpfs,
  }

  Package[$hadoop::packages_httpfs] -> ::Hadoop_lib::Postinstall['hadoop-httpfs']
}

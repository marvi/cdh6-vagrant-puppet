Set-WinUserLanguageList -LanguageList sv-SE -Force

Remove-DnsServerForwarder -IPAddress 192.168.1.1 -Force
Remove-DnsServerForwarder -IPAddress 192.168.1.2 -Force
Add-DnsServerForwarder -IPAddress 1.1.1.1
Add-DnsServerForwarder -IPAddress 8.8.8.8 

Remove-DnsServerZone "55.211.10.in-addr.arpa" -Force
Add-DnsServerPrimaryZone -NetworkId "10.211.55.0/24" -Zonefile "0.55.211.10.in-addr.arpa.dns"

$servers = @(
             @{hostname="master1"; ipv4="10.211.55.90";},
	         @{hostname="master2"; ipv4="10.211.55.91";},
	         @{hostname="worker1"; ipv4="10.211.55.101";},
	         @{hostname="worker2"; ipv4="10.211.55.102";},
	         @{hostname="hue"; ipv4="10.211.55.110";}
            )
           
foreach($server in $servers) {
    Remove-DnsServerResourceRecord -Zonename "sky.net" -RRtype "A" -Name $server.hostname -RecordData $server.ipv4 -Force
    Add-DnsServerResourceRecordA -IPv4Address $server.ipv4  -CreatePtr -Name $server.hostname -ZoneName "sky.net" 
}



$quorum_hostnames = ["master1.${domain}","master2.${domain}","worker1.${domain}" ]

node /^worker1\..*?$/ {
   include common
   include profiles::workernode
   include profiles::hive_worker
   include profiles::journalnode
}

node /^worker2\..*?$/ {
  include common
  include profiles::workernode
  include profiles::hive_worker
}


node /^master1\..*?$/ {
   include common
   #include profiles::namenode1
   #include profiles::hive_metastore
}

node /^master2\..*?$/ {
  include common
  include profiles::namenode2
}


node /^hue\..*?$/ {
  include common
  include profiles::hadoop_common
  include hadoop::frontend
  include profiles::hue
}

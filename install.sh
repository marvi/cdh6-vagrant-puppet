#!/bin/sh

rpm -Uvh https://yum.puppet.com/puppet5/puppet5-release-el-7.noarch.rpm

yum install puppet-agent -y

rpm --import /vagrant/RPM-GPG-KEY-cloudera

cp /vagrant/cloudera-cdh6.repo /etc/yum.repos.d/cdh6.repo

#reboot

rm /etc/hosts
touch /etc/hosts

/opt/puppetlabs/bin/puppet apply /vagrant/manifests/site.pp --codedir /vagrant
